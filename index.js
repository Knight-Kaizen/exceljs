const express = require('express')
const Influx = require('influx');
const excelJS = require('exceljs');

const app = express();
app.use(express.json());

const port = 8001;

const influx = new Influx.InfluxDB({
    host: 'localhost',
    database: 'excelInflux',
    username: "kaizen",

    password: "influx@123",
});


app.post('/influx/write', (req, res) => {
    console.log('checking body', req.body);
    const {sensor, eTime, sTime, value} = req.body;
    influx.writePoints([

        {

            measurement: "sensorData1",

            tags: { sensor: `${sensor}` },

            fields: { sTime: `${sTime}`, eTime: `${eTime}`, value: `${value}` },

            timestamp: Date.now(),

        },

    ])

        .then(() => {

            console.log("Data saved to InfluxDB");
            res.send('Data saved to DB')

        })

        .catch((err) => {

            console.error("Error writing data to InfluxDB:", err);
            res.send('Error in saving data');

        });

    
})

app.get('/influx/read', async (req, res)=>{
    try{
        const data = await influx.query(`SELECT * FROM sensorData1`);
        // console.log(data);
        

        const workbook = new excelJS.Workbook();
        const sheet = workbook.addWorksheet("influxData");
        sheet.columns = [
            {header: 'Sensor', key: 'sensor', width: 25},
            {header: 'Start Time', key: 'sTime', width: 25},
            {header: 'End Time', key: 'eTime', width: 25},
            {header: 'Value', key: 'value', width: 25},
        ];
        
        await  data.map((item)=>{
            console.log('item', item);
            sheet.addRow({
                sensor : item.sensor,
                sTime : item.sTime,
                eTime : item.eTime, 
                value : item.value
            });
        });

        res.setHeader(
            "Content-Type",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        );
        res.setHeader(
            "Content-Disposition",
            "attachment;filename=" + "influxData.xlsx"
        )
        
        workbook.xlsx.write(res);
        
        
        
    }
    catch(err){
        console.log('Error in receiving data', err);
        res.send('error in receiving data ', err);
    }
    
})


app.listen(port, () => {
    console.log('listening to port ', port);
})